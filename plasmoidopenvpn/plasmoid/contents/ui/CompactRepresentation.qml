import QtQuick 2.0
import QtQuick.Controls 2.0
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import QtQuick.Dialogs 1.1
import org.kde.plasma.components 3.0 as PlasmaComponents3
import QtQuick.Layouts 1.1

Item {

    MouseArea {
        id: mousearea
        hoverEnabled: true
        anchors.fill : parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton

      PlasmaCore.IconItem {
        id: playIcon
        width: parent.width
        height: parent.height
      }

        onClicked: {
            if (mouse.button == Qt.RightButton)
                commandResultsDS.exec("sudo pkill openvpn");
            else{
                if(playIcon.source != "test"){
                    commandResultsDS.exec("sudo openvpn --config ~/Documents/VPN/VPN.ovpn --ca ~/Documents/VPN/ca.crt --cert ~/Documents/VPN/user.crt --key ~/Documents/VPN/user.key");
                }
            }
            updateNowPlayingSong();
        }
       
    }

    Component.onCompleted: {
        updateNowPlayingSong();
    }

     Connections {
        target: commandResultsDS
        onExited: {
            var shellResult = stdout.split('\n');
            playIcon.source = shellResult[0];
        }
    }

}
