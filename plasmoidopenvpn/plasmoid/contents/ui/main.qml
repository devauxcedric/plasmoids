import QtQuick 2.0
import QtQuick.Controls 2.0
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import QtQuick.Dialogs 1.1
import org.kde.plasma.components 3.0 as PlasmaComponents3

Item {

   Plasmoid.preferredRepresentation: Plasmoid.compactRepresentation
   Plasmoid.compactRepresentation: CompactRepresentation {}

    function isConstrained() {
        return (plasmoid.formFactor == PlasmaCore.Types.Vertical || plasmoid.formFactor == PlasmaCore.Types.Horizontal);
    }

    // DataSource for the user command execution results
    PlasmaCore.DataSource {
        id: commandResultsDS
        engine: "executable"
        connectedSources: []
        onNewData: {
            var stdout = data["stdout"]
            if (stdout != ""){
                exited(sourceName, stdout)
            }
                disconnectSource(sourceName) // cmd finished
        }

        function exec(cmd) {
                connectSource(cmd)
        }
        signal exited(string sourceName, string stdout)

    }

    Component.onCompleted: {
        updateNowPlayingSong();
        timer.running = true;
    }

    Timer {
        id: timer
        interval: 1000
        running: false
        repeat: true
        onTriggered: updateNowPlayingSong()
    }

     function updateNowPlayingSong(){
        commandResultsDS.exec("
            status=$(echo 'state' | socat - UNIX-CONNECT:/dev/openvpn)
            if [ $? -ne 0 ]
            then
             echo 'unlock'
             exit
            fi
            
            grepped=$(echo $status | grep -o 'CONNECTED')   
            if [ $grepped = 'CONNECTED' ] 
            then
              echo 'lock'
            else
               echo 'view-refresh'
            fi
          ")
     }
}
