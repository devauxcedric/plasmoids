# OpenVPNPlasmoid

A simple kde plasmoid allowing me to control my openVPN without the network-manager

# Installation
```
git clone https://gitlab.com/devauxcedric/openvpnplasmoid
cd vpn-plasmoid
kpackagetool5 -t Plasma/Applet -i ./plasmoid
```

# Update
```
kpackagetool5 -t Plasma/Applet -u ./plasmoid
```
