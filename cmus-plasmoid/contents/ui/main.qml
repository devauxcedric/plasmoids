import QtQuick 2.0
import QtQuick.Controls 2.0
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import QtQuick.Dialogs 1.1
import org.kde.plasma.components 3.0 as PlasmaComponents3

Item {

   Plasmoid.preferredRepresentation: isConstrained() ? Plasmoid.compactRepresentation : Plasmoid.fullRepresentation
   Plasmoid.compactRepresentation: CompactRepresentation {}
   Plasmoid.fullRepresentation: FullRepresentation {}

    function isConstrained() {
        return (plasmoid.formFactor == PlasmaCore.Types.Vertical || plasmoid.formFactor == PlasmaCore.Types.Horizontal);
    }

    // DataSource for the user command execution results
    PlasmaCore.DataSource {
        id: commandResultsDS
        engine: "executable"
        connectedSources: []
        onNewData: {
            var stdout = data["stdout"]
            if (stdout != ""){
                exited(sourceName, stdout)
            }
                disconnectSource(sourceName) // cmd finished
        }

        function exec(cmd) {
                connectSource(cmd)
        }
        signal exited(string sourceName, string stdout)

    }

    Component.onCompleted: {
        updateNowPlayingSong();
        timer.running = true;
    }

    Timer {
        id: timer
        interval: 1000
        running: false
        repeat: true
        onTriggered: updateNowPlayingSong()
    }

     function updateNowPlayingSong(){
        commandResultsDS.exec("
            title=$(cmus-remote -Q | grep title | awk -F 'tag title ' '{print $2}')
            album=$(cmus-remote -Q | grep album | awk -F 'tag album ' '{print $2}')
            artist=$(cmus-remote -Q | grep artist | awk -F 'tag artist ' '{print $2}')
            echo $artist: $album
            echo $title

            status=$(cmus-remote -Q | grep status | awk -F 'status ' '{print $2}')
            if [ $status = 'stopped' ] ||  [ $status = 'paused' ]
            then
              echo 'media-playback-start'
            else
               echo 'media-playback-pause'
            fi
          ")
     }
}
