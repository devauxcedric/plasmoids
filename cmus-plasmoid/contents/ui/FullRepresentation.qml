import QtQuick 2.0
import QtQuick.Controls 2.0
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import QtQuick.Dialogs 1.1
import org.kde.plasma.components 3.0 as PlasmaComponents3
import QtQuick.Layouts 1.1

Item {
    Layout.preferredWidth: 200
    Layout.preferredHeight: 80

     Component.onCompleted: {
        updateNowPlayingSong();
    }

    Connections {
        target: commandResultsDS
        onExited: {
            var shellResult = stdout.split('\n');
            subtitle.text = shellResult[0]
            title.text = shellResult[1];
            playButton.icon.name = shellResult[2];
        }
    }

    PlasmaComponents3.Label {
        id: title
        text: "Loading"
        anchors.horizontalCenter: parent.horizontalCenter;
        anchors.bottom: subtitle.top;
    }

    PlasmaComponents3.Label {
        id: subtitle
        text: "Loading"
        anchors.horizontalCenter: parent.horizontalCenter;
        anchors.bottom: mediaControll.top;
    }

    function executeCommand(command){
        commandResultsDS.exec(command);
        updateNowPlayingSong();
    }

    Row {
        id: mediaControll
        anchors.horizontalCenter: subtitle.horizontalCenter;
        anchors.bottom: parent.bottom;


        PlasmaComponents3.ToolButton {
            anchors.verticalCenter: parent.verticalCenter
            icon.name: "media-skip-backward"
            onClicked: executeCommand("cmus-remote -r");
        }

        PlasmaComponents3.ToolButton {
            anchors.verticalCenter: parent.verticalCenter
            id: playButton
            icon.name: "media-playback-start"
            onClicked: executeCommand("cmus-remote -u");
        }

        PlasmaComponents3.ToolButton {
            anchors.verticalCenter: parent.verticalCenter
            icon.name: "media-skip-forward"
            onClicked: executeCommand("cmus-remote -n");
        }
    }

}
