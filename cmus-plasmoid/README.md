# Cmus-plasmoid

Provide a way to control a cmus instance directly in KDE. It use the cmus-remote utility of cmus.

# Installation

```bash
git clone https://gitlab.com/devauxcedric/cmus-plasmoid
cd cmus-plasmoid/
sudo plasmapkg2 -g -i plasmoid
```
   
If updating:

```bash
sudo plasmapkg2 -g -u plasmoid
```
